package th.ac.tu.siit.lab9googlemaps;

import java.util.Random;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends FragmentActivity
	implements ConnectionCallbacks,OnConnectionFailedListener, LocationListener {
	
	GoogleMap map;
	Location currentLocation;
	LocationClient locClient;
	LocationRequest locRequest;
	int color = Color.BLACK;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		MapFragment mapFragment = 
				(MapFragment)getFragmentManager().findFragmentById(R.id.map);
		map = mapFragment.getMap();
		map.setMyLocationEnabled(true);
		map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);// specify the map type, if commend this one it will return the different picture
		locClient = new LocationClient(this, this, this);
		
		//we can draw something on the map
		/*MarkerOptions m = new MarkerOptions();
		m.position(new LatLng(0,0)); //latitude longtidude
		map.addMarker(m);
		
		PolylineOptions p = new PolylineOptions();
		p.add(new LatLng(0,0));
		p.add(new LatLng(13.75, 100.4667));
		p.width(10);
		p.color(Color.BLUE);
		map.addPolyline(p);
		*/
	}
	
	protected void onStart() {
		super.onStart();
		locClient.connect();
	}
	
	protected void onStop() {
		locClient.disconnect();
		super.onStop();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case R.id.action_mark:
			
			MarkerOptions m = new MarkerOptions();
			m.position(new LatLng(locClient.getLastLocation().getLatitude(), locClient.getLastLocation().getLongitude()));
			map.addMarker(m);
		
			break;
	
		case R.id.action_change:
			color = getRandomColor();
			
			break;
		default:
			return super.onOptionsItemSelected(item);

		}
		return true;
	}
	
	

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		
	}

	@Override
	public void onConnected(Bundle arg0) { //when you connect to the internet
		currentLocation = locClient.getLastLocation();

		locRequest = LocationRequest.create();
		locRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		locRequest.setInterval(5*1000);
		locRequest.setFastestInterval(1*1000);
		locClient.requestLocationUpdates(locRequest, this);
	}

	@Override
	public void onDisconnected() {
		
	}
	
	private int getRandomColor() {
		Random r = new Random();
		float[] hsv = new float[3];
		hsv[1] = 1.0f; //saturation
		hsv[2] = 1.0f;// brightness value
		hsv[0] = r.nextInt(360);
		int c = Color.HSVToColor(hsv);
		return c;

	}


	@Override
	public void onLocationChanged(Location l) { //when you get the new location
		if (currentLocation.distanceTo(l) > 0.01) {
			
			
			/*MarkerOptions m = new MarkerOptions();
			m.position(new LatLng(l.getLatitude(), l.getLongitude()));
			map.addMarker(m);*/
			PolylineOptions p = new PolylineOptions();
			p.add(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
			p.add(new LatLng(l.getLatitude(),l.getLongitude()));
			
			p.width(10);
			p.color(color);
			map.addPolyline(p);
			
			
			currentLocation = l;
		}
	}
}
